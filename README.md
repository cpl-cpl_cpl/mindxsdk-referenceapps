EN|[CN](README.zh.md)
# MindX SDK Reference Apps

[MindX SDK](https://www.hiascend.com/software/mindx-sdk) is a software development kit (SDK) launched by Huawei. It provides simplified high-performance APIs and tools to enable Ascend AI processors in various application scenarios. mxSdkReferenceApps is a reference sample developed based on MindX SDK.


## Contribution

Refer to [CONTRIBUTING.md](contrib/CONTRIBUTING.md).

## Copyright Description

Refer to [License.md](License.md).

