# 模型后处理插件开发调试指导
本章节将指导在SDK自带的图像分类识别样例中，使用支持类型的自定义模型后处理生成so库，并在IDE中加载单步调试的流程。  
在执行本样例前，应已成功远程部署并运行SDK自带的图像分类识别样例。  
[点击跳转代码样例](https://gitee.com/ascend/mindxsdk-referenceapps/tree/master/tutorials/SamplePostProcess)

## 插件说明
[模型后处理介绍](https://support.huaweicloud.com/ug-vis-mindxsdk201/atlasmx_02_0069.html)  
模型后处理为与模型一一对应的配套操作，在SDK中其主要工作是用于对模型推理插件传入的推理结果张量进行处理，如在目标检测任务中，需要对目标框进行去重，排序，筛选等。最后将处理结果写入目标信息的类对象中，并传递回给目标检测后处理插件进行写入元数据以传递给下游插件。  

[模型支持列表](https://support.huaweicloud.com/ug-vis-mindxsdk201/atlasmx_02_0129.html)  
目前SDK所支持的模型均已开发相应的后处理，在pipeline中指定相关so文件位置即可使用。如果现有后处理无法兼容用户模型，用户应自行开发后处理。  

[后处理开发步骤](https://support.huaweicloud.com/ug-vis-mindxsdk201/atlasmx_02_0070.html)  
>根据任务类型，选择SDK已经支持的后处理基类去派生一个新的子类，这些后处理基类分别为目标检测，分类任务，语义分割，文本生成。这些基类都继承自相同的父类——PostProcessBase。在Init函数中，需要调取父类的Init()接口读取配置参数，然后再调用父类对象的configData_.GetFileValue()接口读取子类所需要的配置参数。在Process函数中，以模型的输出张量——tensors作为输入，以相应的数据结构类对象作为输出。首先调用父类的CheckAndMoveTensors()接口，对tensors的形状进行校验并搬运内存至Host侧。然后再进行相应操作获得结果。后处理开发完成后，增加一个对外的接口如GetObjectInstance()，以便于让业务流中的后处理插件动态加载此后处理so。  
如果当前后处理基类所采用的数据结构无法满足需求，可以新增后处理基类继承PostProcessBase，并写入新的数据结构。

****
**注意！**  
该后处理方式需要继承特定后处理基类(位于MxBase/PostProcessBases/*)，并使用对应的对外接口。支持种类参考上述模型支持列表中的对应关系。

yolov3样例属于ObjectPostProcessBase，使用GetObjectInstance

resnet50样例属于ClassPostProcessBase，使用GetClassInstance

导出示例分别为：
```C++
    extern "C" {
    std::shared_ptr<MxBase::Yolov3PostProcess> GetObjectInstance();
    }

    extern "C" {
    std::shared_ptr<MxBase::SamplePostProcess> GetClassInstance();
    }
```
****

## 项目准备

**步骤1** 准备相关工程文件  
在clion中创建新工程，该工程应该为样例的顶级目录（本样例中使用mxVision，mxManufacture替换对应名称即可，样例代码通用）

指定对应的远程开发目录  
![image.png](img/202107131357.png 'image.png')  
删除工程自带的main.cpp文件，并将已经之前成功部署并运行SDK自带的图像分类识别样例的文件夹下载至本地。
>**注意** model目录下需包含yolov3和resnet50模型文件

该步骤完成后的目录结构如下：  
![image.png](img/202107131607.png 'image.png')   

**步骤2** 测试图像分类识别样例  
参考[4-1](4-1插件开发调试指导.md)中相同章节内容  
所有项目均需设置以下环境变量  
![image.png](img/202107131650.png 'image.png')  
本样例的顶层Cmakelist文件参考如下：
```cmake
# Copyright(C) 2021. Huawei Technologies Co.,Ltd. All rights reserved.
cmake_minimum_required(VERSION 3.5.2)

project(mxpi_sampleplugin)

#该语句中%MX_SDK_HOME%根据实际SDK安装位置修改，可通过在终端运行env命令查看
set(MX_SDK_HOME %MX_SDK_HOME%)

add_subdirectory("./mxVision/C++")
add_subdirectory("./src/samplepostprocess")
add_subdirectory("./src/yolov3")
```
配置子项目并成功运行测试sample后进入下一步

**步骤3** 加载自定义的模型后处理  
配置Clion项目并加载和编译后处理库yolov3postprocess和samplepostprocess(resnet50)，下图为切换至samplepostprocess项目  
![image.png](img/202107131627.png 'image.png')  
更改mxVision/C++/main.cpp中94行所使用的pipeline为样例中的Sample_new.pipeline  
>该文件位于样例根目录，但代码中实际指向mxVision/pipeline文件夹下，这是为了与使用原有Sample.pipeline的样例统一目录。实际使用时复制pipeline文件或更改代码中的路径均可  

该文件使用了新框架模型后处理方式并指向位于项目根目录lib文件夹下的编译的后处理库so文件  
>有关新旧框架模型后处理的差异请参见FAQ和SDK用户手册  

![image.png](img/202107131630.png 'image.png')  
切换回main项目并运行，result输出结果应该与初始（旧模型后处理配置方式+SDK内置后处理so文件的Sample.pipeline）相同  

 **步骤4** 单步调试模型后处理  
 切换为需要调试的项目，在插件代码中打断点，并debug运行 
 ![image.png](img/202107131639.png 'image.png')  
 >注意：需要调试时请注释cmakelist该行内容，这是用于安全编译设置的选项。但正式提交时请保持本行生效。
 ```cmake
 target_link_libraries(${TARGET_LIBRARY} -Wl,-z,relro,-z,now,-z,noexecstack -s)
 ```